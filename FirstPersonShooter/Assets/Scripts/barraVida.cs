﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barraVida : MonoBehaviour
{
    public Slider slider;
    public void VidaMaxima(float vida)
    {
        slider.maxValue = vida;
        slider.value = vida;
    }
    public void VidaActual(float vida)
    {
        slider.value = vida;
    }
}
