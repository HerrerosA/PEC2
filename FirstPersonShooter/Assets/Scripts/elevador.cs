﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class elevador : MonoBehaviour
{
    public Transform abajo;
    public Transform arriba;
    public Transform plataforma;
    public float speed = 1.0f;
    
    bool estaSubiendo = false;
    bool estaBajando = false;
    
    Vector3 distancia;

    void Update(){
        if(estaSubiendo){
            distancia = plataforma.localPosition - arriba.localPosition;
            if (distancia.magnitude < 0.001f){
                estaSubiendo = false;
                plataforma.localPosition = arriba.localPosition;
            }
            else{
                plataforma.localPosition = Vector3.Lerp(plataforma.localPosition, arriba.localPosition, Time.deltaTime * speed);
            }
        }
        else if(estaBajando){
            distancia = plataforma.localPosition - abajo.localPosition;
            if (distancia.magnitude < 0.001f){
                estaBajando = false;
                plataforma.localPosition = abajo.localPosition;
            }
            else{
                plataforma.localPosition = Vector3.Lerp(plataforma.localPosition, abajo.localPosition, Time.deltaTime * speed);
            }
        }
    }
    void OnTriggerEnter(Collider col){
        if (col.gameObject.tag == "Player"){
            estaSubiendo = true;
            estaBajando = false;
           
        }
    }
    void OnTriggerStay(Collider col){
        if (col.gameObject.tag == "Player"){
                estaSubiendo = true;
                estaBajando = false;
        }
    }
    void OnTriggerExit(Collider col)
    {
        
        estaSubiendo = false;
        estaBajando = true;
    }


}
