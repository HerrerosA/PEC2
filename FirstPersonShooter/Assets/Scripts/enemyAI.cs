﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class enemyAI : MonoBehaviour
{
    [HideInInspector] public PatrolState patrolState;
    [HideInInspector] public AlertState alertState;
    [HideInInspector] public AttackState attackState;
    [HideInInspector] public IEnemyState currentState;

    [HideInInspector] public NavMeshAgent navMeshAgent;

    public GameObject explosion;
    public GameObject disparo;

    public AudioSource fireSound;
    public Light myLight;
    public float life = 100;
    public float timeBetweenShoots = 1.0f;
    public float damageForce = 10f;
    public float rotationTime = 3.0f;
    public float shootHeight = 0.5f;
    public Transform[] wayPoints;
    public GameObject[] objetos;
    private float tiempoDesdeExplosion = 0;
    private float tiempoDesdeDisparo = 0;
    private int siguienteObjeto = 0;
    
    void Start()
    {
        // Creamos los estados de nuestra IA.
        patrolState = new PatrolState(this);
        alertState = new AlertState(this);
        attackState = new AttackState(this);


        // Le decimos que inicialmente empezará patrullando
        currentState = patrolState;

        // Guardamos la referencia de nuestro NavMesh Agent
        navMeshAgent = GetComponent<NavMeshAgent>();

        
    }

    void Update()
    {

        // Como nuestros estados no heredan de MonoBehaviour, no se llama a su update automáticamente, y nos encargaremos nosotros de llamarlo a cada frame.
        currentState.UpdateState();
        if (disparo.activeSelf == true){
            tiempoDesdeDisparo += Time.deltaTime;
            if (tiempoDesdeDisparo > 0.5){
                disparo.SetActive(false);
            }
        }
        
        // Morir
        if (life <= 0){
            explosion.SetActive(true);
            tiempoDesdeExplosion += Time.deltaTime;
            if (tiempoDesdeExplosion > 0.5){
                if (objetos.Length>0){
                    for(int i = 0; i < objetos.Length; i++)
                    {
                        
                        Instantiate(objetos[i], new Vector3(transform.position.x + i*0.5f, transform.position.y + i*0.5f, transform.position.z), Quaternion.identity);
                    }
                }
                Destroy(this.gameObject);
            }
            
        }         
    }

    // Cuando el player les dispara, les quitamos vida y avisa al estado en que esté de que le han disparado
    public void Hit(float damage)
    {
        tiempoDesdeDisparo = 0;
        life -= damage;
        currentState.Impact();
        disparo.SetActive(true);
        
        
    }

    // Ya que los states no heredan de MonoBehaviour, hay que avisarles cuando algo entra, está o sale del trigger.

    
    void OnTriggerEnter(Collider col){
        currentState.OnTriggerEnter(col);
    }
    void OnTriggerStay(Collider col){
        currentState.OnTriggerStay(col);
    }
    void OnTriggerExit(Collider col){
        currentState.OnTriggerExit(col);
    }
}
