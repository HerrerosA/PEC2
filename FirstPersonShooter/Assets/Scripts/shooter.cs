﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class shooter : MonoBehaviour
{
    public GameObject decalPrefab;
    public AudioSource fireSound;
    public GameObject pistola;
    public GameObject ametralladora;
    public barraVida barraVida;
    public barraEscudo barraEscudo;
    public Text municion;
    public Text llavesTotal;

    public GameObject mensajeLlaves;

    GameObject[] totalDecals;
    int actual_decal = 0;

    public float vidaMaxima = 100;
    public float vidaActual;
    public float escudoMaximo = 100;
    public float escudoActual;
    public float damageForce = 15f;
    public int balas = 30;
    public int llaves = 0;
    private float timeBetweenShoots = 1.0f;
    private float actualTimeBetweenShoots = 0;


    void Start()
    {
        vidaActual = vidaMaxima;
        barraVida.VidaMaxima(vidaMaxima);
        escudoActual = escudoMaximo;
        barraEscudo.EscudoMaximo(escudoMaximo);
        totalDecals = new GameObject[10];
        pistola.SetActive(true);
        ametralladora.SetActive(false);
        timeBetweenShoots = 1.0f;
        
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            pistola.SetActive(true);
            ametralladora.SetActive(false);
            timeBetweenShoots = 1.0f;
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            pistola.SetActive(false);
            ametralladora.SetActive(true);
            timeBetweenShoots = 0.1f;
        }
        actualTimeBetweenShoots += Time.deltaTime; 
        
        if(Input.GetMouseButton(0)){
            if (actualTimeBetweenShoots > timeBetweenShoots && balas > 0){
                actualTimeBetweenShoots = 0;
                balas -= 1;
                fireSound.Play();
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0)), out hit))
                {
                    
                    if(hit.collider.gameObject.tag == "Enemigo"){
                        hit.collider.GetComponentInParent<enemyAI>().Hit(damageForce);
                    }
                    else{
                        Destroy(totalDecals[actual_decal]);
                        totalDecals[actual_decal] = GameObject.Instantiate(decalPrefab, hit.point + hit.normal * 0.01f, Quaternion.FromToRotation(Vector3.forward, -hit.normal)) as GameObject;
                        actual_decal++;
                        if (actual_decal == 10) actual_decal = 0;
                    }
                }
                municion.text = balas.ToString("00");
            }
            
        }
        if (vidaActual <= 0){
            SceneManager.LoadScene("GameOver");
        }
    }
    public void Hit(float damage)
    {
        if (escudoActual > 0){
            escudoActual -= damage*0.8f;
            vidaActual -= damage*0.2f;
        }
        else{
            vidaActual -= damage;
        }
        barraEscudo.EscudoActual(escudoActual);
        barraVida.VidaActual(vidaActual);
    }
    public void CogerMunicion(){
        balas = balas + 10;
        municion.text = balas.ToString("00");
    }
    public void CogerVida(){
        vidaActual = vidaActual + 10;
        if (vidaActual > vidaMaxima){
            vidaActual = vidaMaxima;
        }
        barraVida.VidaActual(vidaActual);
        
    }
    public void CogerEscudo(){
        escudoActual = escudoActual + 10;
        if (escudoActual > escudoMaximo){
            escudoActual = escudoMaximo;
        }
        barraEscudo.EscudoActual(escudoActual);
    }
    public void CogerLlave(){
        llaves = llaves + 1;
        ActualizarLlaves();
    }
    public void ActualizarLlaves(){
        llavesTotal.text = llaves.ToString("0");
    }

}
