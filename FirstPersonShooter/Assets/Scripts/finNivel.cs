﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class finNivel : MonoBehaviour
{
    public string nivelSiguiente;
    
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player"){
       
            IniciarNivel();
        }
    
    }

    private void IniciarNivel(){
        SceneManager.LoadScene(nivelSiguiente);
    }
}
