﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class gameOver : MonoBehaviour
{
    public Text contador;
    public int tiempo=5;
    private float tiempoDesdeGameOver;
    void Start(){
        contador.text = tiempo.ToString("0");
    }
    void Update(){
        contador.text = (tiempo - Time.timeSinceLevelLoad).ToString("0");
        if (Time.timeSinceLevelLoad >= 5){
            Reiniciar();
        }
    }

    private void Reiniciar(){
        SceneManager.LoadScene("Nivel1");
    }
}
