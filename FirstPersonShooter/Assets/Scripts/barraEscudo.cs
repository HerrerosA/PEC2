﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class barraEscudo : MonoBehaviour
{
    public Slider slider;
    public void EscudoMaximo(float escudo)
    {
        slider.maxValue = escudo;
        slider.value = escudo;
    }
    public void EscudoActual(float escudo)
    {
        slider.value = escudo;
    }
}
