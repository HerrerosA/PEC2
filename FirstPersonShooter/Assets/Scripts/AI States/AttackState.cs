﻿using UnityEngine;
using System.Collections;

public class AttackState : IEnemyState
{
    enemyAI myEnemy;
    float actualTimeBetweenShoots = 0;
    Collider colision;

    // Cuando llamamos al construcotr, guardamos una referencia a la IA de nuestro enemigo
    public AttackState(enemyAI enemy)
    {
        myEnemy = enemy;
    }
    // Aquí va toda la funcionalidad que queramos que haga el enemigo cuando esté en este estado
    public void UpdateState()
    {
        myEnemy.myLight.color = Color.red;
        actualTimeBetweenShoots += Time.deltaTime;  
        // Si lleva más de dos tiempos sin disparar vuelve al modo alerta porque significa que el jugador está fuera de su alcance 
        if (actualTimeBetweenShoots > (myEnemy.timeBetweenShoots)*2)
        {
            GoToAlertState();
        }     
    }


    // Si el player nos ha disparado no haremos nada.
    public void Impact() {}
    // Como ya estamos en el estado Attack no llamaremos nunca a estas funciones desde este estado
    public void GoToAttackState(){}
    public void GoToPatrolState(){}
    public void GoToAlertState(){
        myEnemy.currentState = myEnemy.alertState;
    }

    //El player ya está en nuestro trigger
    public void OnTriggerEnter(Collider col){}

    //Orientamos al enemigo mirando siempre al player mientras le ataquemos
    public void OnTriggerStay(Collider col)
    {
        // Estaremos mirando siempre al player
        Vector3 lookDirection = col.transform.position - myEnemy.transform.position;

        //Rotando solamente en el eje Y
        myEnemy.transform.rotation = Quaternion.FromToRotation(Vector3.forward, new Vector3(lookDirection.x, 0, lookDirection.z));

        // Le toca volver a disparar
        if(actualTimeBetweenShoots > myEnemy.timeBetweenShoots)
        {
            actualTimeBetweenShoots = 0;
            if(col.gameObject.tag == "Player"){
                col.gameObject.GetComponent<shooter>().Hit(myEnemy.damageForce);
            }
            myEnemy.fireSound.Play();
        }
        
    }
    public void OnTriggerExit(Collider col)
    {
        GoToAlertState();
    }
}
