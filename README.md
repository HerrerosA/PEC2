# 3D - PEC2

## CÓMO JUGAR
Para jugar se iniciará en la escena "Nivel 1".

El jugador empieza directamente el juego en primera persona y los controles son los siguientes:
- tecla W: desplazarse hacia adelante
- tecla S: desplazarse hacia atrás
- tecla A: desplazarse a la izquierda
- tecla D: desplazarse a la derecha
- tecla barra espaciadora: saltar
- ratón: con los movimientos de ratón se moverá la cámara simulando hacia dónde mira el jugador.
- botón izquierdo ratón: disparar
- tecla 1: seleccionar la pistola
- tecla 2: seleccionar la ametralladora

Una vez iniciado el juego se puede ver en pantalla la siguiente información:
- barra escudo: es de color morado y está situada arriba a la izquierda
- barra vida: es de color rojo y está situada debajo de la barra escudo
- munición: está situada arriba a la derecha y muestra el número de balas disponibles
- llaves: está debajo de la munición y muestra el número de llaves recolectadas y no utilizadas
- arma: se puede ver el cañon del arma seleccionada en la parte de abajo de la pantalla

Si un enemigo alcanza al jugador ese daño se repartirá entre un 80% para el escudo y un 20% para la vida. Una vez el escudo se haya agotado, todo el daño lo recibirá la vida.
Se pueden ir recolectando objetos esparcidos por el escenario o bien tras eliminar a un enemigo que puede lanzar algunos objetos al morir.
Existen 4 tipos de objetos:
- botiquín: sumará un 10% a la barra de vida
- chaleco antibalas: sumará un 10% a la barra de escudo
- munición: sumará 10 balas a la munición
- llave: sumará una llave a la cantidad de llaves no utilizadas

A lo largo de los niveles existen diferentes puertas que se abren automáticamente o precisan de una cantidad concreta de llaves para poder abrirlas.

También existe una plataforma elevadora que al detectar que el jugador se encuentra encima subirá para permitirle seguir avanzando. Una vez el jugador se baje de la plataforma esta volverá a su posición inicial.

Cuando el jugador llega al final del nivel pasa automáticamente al siguiente, y si la vida llega a 0 se podrá ver la pantalla de Game Over y se reniciará de nuevo desde el principio.


## ESTRUCTURA E IMPLEMENTACIÓN

# Enemigo
Los enemigos están formados por un elemento Drone ya predefinido al que se le ha incorporado una luz en el morro que cambia según el estado en el que se encuentre, un GameObject de dispario y otro de explosión, además tiene un collider tipo esfera.
Este elemento tiene una pequeña máquina de estados definida en diversos scripts que le proporcionan una cierta inteligencia artificial. Los tres estados posibles son: búsqueda (luz verde), alerta (luz amarilla) y ataque (luz roja).
En el script hay un array de las posiciones por las que deberá buscar o patrullar el enemigo en el que hay que definir la cantidad y añadir cada transform con las posiciones deseadas.
También hay un array con los objetos recolectables que lanzará tras ser eliminado.
Si se encuentra en modo búsqueda irá volando por las posiciones definidas. Si se encuentra en modo alerta, dará una vuelta de 360º para ver si encuentra al jugador y pasa al modo ataque. Si se encuentra en modo ataque, bien porque el jugador le ha disparado o bien porque haya entrado en el collider de la esfera que posee el enemigo, irá disparando al jugador y emitirá sonido de disparo, si pasa demasiado tiempo entre un disparo y otro, porque ya no puede ver al jugador, volverá al modo búsqueda.
Cuando el jugador alcance al enemigo de un disparo se podrán ver unas pequeñas chispas que indican que ha habido impacto y cuando le haya restado toda la vida se producirá una explosión.

# Objetos recolectables
Como se ha indicado anteriormente existen 4 tipos de objetos pero todos comparten prácticamente las mismas propiedades. Cuando el jugador entra en su collider, este objeto se sumará al jugador y desaparecerá.

# Puertas
Cuando el jugador entre en su collider la puerta tratará de desplazar las dos hojas para que quede abierta hasta que el jugador se aleje, pero si precisa de llaves, lo que pasará es que comprobará si el jugador dispone de ellas y se las restará a este si es así, quedando la puerta desbloqueada desde ese momento.

# Elevador
El funcionamiento es similar al de la puerta, cuando el jugador entra en el collider, se desplaza de una a otra posición definida, igual que ocurre con las hojas de la puerta, y cuando el jugador sale, la plataforma vuelve a su posición original.

# Elementos en pantalla
Los elementos permanentes en la pantalla que se han definido anteriormente se actualizarán mediante sus scripts correspondientes o el del jugador según sea necesario.

# Sonidos
Se han añadido algunos efectos de sonido a ciertos elementos como los disparos, el vuelo del drone o la explosión del enemigo.

